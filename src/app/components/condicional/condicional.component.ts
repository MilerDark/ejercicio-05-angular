import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-condicional',
  templateUrl: './condicional.component.html',
  styleUrls: ['./condicional.component.css']
})
export class CondicionalComponent implements OnInit {
  informacion: any = {
  nombre: "Miler Arnez",
  direccion: "Av. Circunvalacion"
  };

  mostrar: boolean = true;
  
  constructor() { }

  ngOnInit(): void {
  }

}
